package com.skillabb.msklbpitmtdenvaccessprovider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class MsklbPItmtDenvAccessProviderApplication {

  public static void main(String[] args) {
    SpringApplication.run(MsklbPItmtDenvAccessProviderApplication.class, args);
  }

}
