FROM  openjdk:8-jdk-alpine

LABEL version="1.0.0"
LABEL description="Gateway server for skillabb ms practice"
LABEL copyright="skillabb.com"

RUN apk --no-cache add curl

ARG JAR_FILE=target/gateway.jar

ADD ${JAR_FILE} /app/gateway.jar

CMD ["java", "-Xmx200m", "-jar", "/app/gateway.jar"]
